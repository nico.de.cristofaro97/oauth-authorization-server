-- Bellow here you can find the User and Authority reference SQL schema used by Spring’s org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl.
-- If you want customize attribute for your user you have extends that class and the classes which use it.
USE oauth;

CREATE TABLE IF NOT EXISTS users (
  id INT AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(256) NOT NULL,
  password VARCHAR(256) NOT NULL,
  enabled TINYINT(1),
  site_id INTEGER,
  UNIQUE KEY unique_username(username)
);

CREATE TABLE IF NOT EXISTS authorities (
  username VARCHAR(256) NOT NULL,
  authority VARCHAR(256) NOT NULL,
  PRIMARY KEY(username, authority)
);

-- And then add the following entry

-- The encrypted password is `pass`
INSERT INTO users (id, username, password, enabled, site_id) VALUES (1, 'user@gmail.com', '{bcrypt}$2a$10$cyf5NfobcruKQ8XGjUJkEegr9ZWFqaea6vjpXWEaSqTa2xL9wjgQC', 1,3);
INSERT INTO authorities (username, authority) VALUES ('user@gmail.com', 'ROLE_USER');