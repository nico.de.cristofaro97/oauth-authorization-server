# Authorization Server (OAuth2.0)

Authorization Server component of a system based on OAuth2.0 delegation protocol. 

Here is a quick glossary of OAuth terms (taken from the core spec):

- **Resource owner** (a.k.a. the User) - An entity capable of granting access to a protected resource. When the resource owner is a person, it is referred to as an end-user.
- **Resource server** (a.k.a. the API server) - The server hosting the protected resources, capable of accepting and responding to protected resource requests using access tokens (see project here -> https://gitlab.com/nico.de.cristofaro97/oauth-resource-server.git).
- **Client** - An application making protected resource requests on behalf of the resource owner and with its authorization. The term client does not imply any particular implementation characteristics (e.g. whether the application executes on a server, a desktop, or other devices).
- **Authorization server** - The server issuing access tokens to the client after successfully authenticating the resource owner and obtaining authorization.

## Flow of event : Resource Owner Password Credentials (see the RFC -> https://tools.ietf.org/html/rfc6749#section-4.3)

The flow includes the following steps:
1. The client will ask the user for their authorization credentials (ususally a username and password).
2. The client sends the credentials and its own identification to the authorization server, more specifically the client sends a POST request with following body parameters to the authorization server:
   - ```grant_type``` with the value ```password```
   - ```client_id``` with the the client’s ID
   - ```client_secret``` with the client’s secret
   - ```scope``` with a space-delimited list of requested scope permissions.
   - ```username``` with the user’s username
   - ```password``` with the user’s password
3. The authorization server validates the information, then returns an access token and optionally a refresh token, more specifically thhe authorization server will respond with a JSON object containing the following properties:
   - ```token_type``` with the value ```Bearer```
   - ```expires_in``` with an integer representing the TTL of the access token
   - ```access_token``` the access token itself
   - ```refresh_token``` a refresh token that can be used to acquire a new access token when the original expires
4. The client uses the access token to access resources on the resource server.

![Markdown Plus](https://miro.medium.com/max/1568/0*IsGZosgvxFHqNJYA.)


## Flow of event : Client credentials grant (see the RFC -> https://tools.ietf.org/html/rfc6749#section-4.4)
The client credentials grant type must only be used by confidential clients. 
This grant is suitable for machine-to-machine authentication where a specific user’s permission to access data is not required.

The flow includes the following steps:
1. The client sends a POST request with following body parameters to the authorization server:
   - ```grant_type``` with the value ```client_credentials```
   - ```client_id``` with the the client’s ID
   - ```client_secret``` with the client’s secret
   - ```scope``` with a space-delimited list of requested scope permissions.
2. The authorization server will respond with a JSON object containing the following properties:
   - ```token_type``` with the value ```Bearer```
   - ```expires_in``` with an integer representing the TTL of the access token
   - ```access_token``` the access token itself
3. The client uses the access token to access resources on the resource server (see project here -> https://gitlab.com/nico.de.cristofaro97/oauth-resource-server.git).


![Markdown Plus](https://docs.axway.com/bundle/APIGateway_762_OAuthUserGuide_allOS_en_HTML5/page/Content/Resources/Images/docbook/images/oauth/APIgw_Client_cred_grant_flow.png)