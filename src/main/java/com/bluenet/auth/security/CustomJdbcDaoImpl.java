package com.bluenet.auth.security;

import com.bluenet.auth.security.model.AuthUser;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class CustomJdbcDaoImpl extends JdbcDaoImpl {

    public static final String DEF_USERS_BY_USERNAME_QUERY = "select username,password,enabled,site_id "
            + "from users " + "where username = ?";

    private String usersByUsernameQuery;

    public CustomJdbcDaoImpl() {
        this.usersByUsernameQuery = DEF_USERS_BY_USERNAME_QUERY;
    }

    @Override
    protected List<UserDetails> loadUsersByUsername(String username) {
        return getJdbcTemplate().query(this.usersByUsernameQuery,
                new String[] { username }, new RowMapper<UserDetails>() {
                    @Override
                    public UserDetails mapRow(ResultSet rs, int rowNum)
                            throws SQLException {
                        String username = rs.getString(1);
                        String password = rs.getString(2);
                        boolean enabled = rs.getBoolean(3);
                        int siteId = rs.getInt(4);

                        return new AuthUser(username, password, siteId, enabled,
                                true, true, true,
                                AuthorityUtils.NO_AUTHORITIES);
                    }

                });
    }

    @Override
    protected UserDetails createUserDetails(String username,
                                         UserDetails userFromUserQuery, List<GrantedAuthority> combinedAuthorities) {

        CustomUserDetails authUserFromQuery = (CustomUserDetails)userFromUserQuery;
        return new AuthUser(authUserFromQuery.getUsername(), authUserFromQuery.getPassword(), authUserFromQuery.getSiteId(),
                authUserFromQuery.isEnabled(), true, true, true, combinedAuthorities);


    }
}
