package com.bluenet.auth.security;

import org.springframework.security.core.userdetails.UserDetails;

public interface CustomUserDetails extends UserDetails {

    /**
     * Returns the site_id of the user.
     *
     * @return the site_id
     */
    Integer getSiteId();
}
