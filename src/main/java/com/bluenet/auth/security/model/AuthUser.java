package com.bluenet.auth.security.model;

import com.bluenet.auth.security.CustomUserDetails;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class AuthUser extends org.springframework.security.core.userdetails.User implements CustomUserDetails {

    private static final long serialVersionUID = 1L;

    private Integer siteId;
    private String name;
    private String password;



    public AuthUser(String username, String password, Integer siteId, boolean enabled,
                    boolean accountNonExpired, boolean credentialsNonExpired,
                    boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.siteId = siteId;
        this.setName(username);
        this.setPassword(password);
        //new lines

    }

    public Integer getSiteId() {
        return siteId;
    }


    public String getName() {
        return name;
    }

    @Override
    public String getPassword() {
        return password;
    }


    public void setSiteId(Integer siteId) {
        this.siteId = siteId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}

